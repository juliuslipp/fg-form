// Generated.
export interface CountryType {
  name: string;
  code: string;
  nativeName: string;
  suggested?: boolean;
}

export const COUNTRIES: CountryType[] = [
  {
    code: 'AF',
    name: 'Afghanistan',
    nativeName: 'افغانستان',
  },
  {
    code: 'AX',
    name: 'Åland Islands',
    nativeName: 'Åland',
  },
  {
    code: 'AL',
    name: 'Albania',
    nativeName: 'Shqipëria',
  },
  {
    code: 'DZ',
    name: 'Algeria',
    nativeName: 'الجزائر',
  },
  {
    code: 'AS',
    name: 'American Samoa',
    nativeName: 'American Samoa',
  },
  {
    code: 'AD',
    name: 'Andorra',
    nativeName: 'Andorra',
  },
  {
    code: 'AO',
    name: 'Angola',
    nativeName: 'Angola',
  },
  {
    code: 'AI',
    name: 'Anguilla',
    nativeName: 'Anguilla',
  },
  {
    code: 'AQ',
    name: 'Antarctica',
    nativeName: 'Antarctica',
  },
  {
    code: 'AG',
    name: 'Antigua and Barbuda',
    nativeName: 'Antigua and Barbuda',
  },
  {
    code: 'AR',
    name: 'Argentina',
    nativeName: 'Argentina',
  },
  {
    code: 'AM',
    name: 'Armenia',
    nativeName: 'Հայաստան',
  },
  {
    code: 'AW',
    name: 'Aruba',
    nativeName: 'Aruba',
  },
  {
    code: 'AU',
    name: 'Australia',
    nativeName: 'Australia',
  },
  {
    code: 'AT',
    name: 'Austria',
    nativeName: 'Österreich',
    suggested: true,
  },
  {
    code: 'AZ',
    name: 'Azerbaijan',
    nativeName: 'Azərbaycan',
  },
  {
    code: 'BS',
    name: 'Bahamas',
    nativeName: 'Bahamas',
  },
  {
    code: 'BH',
    name: 'Bahrain',
    nativeName: 'البحرين',
  },
  {
    code: 'BD',
    name: 'Bangladesh',
    nativeName: 'Bangladesh',
  },
  {
    code: 'BB',
    name: 'Barbados',
    nativeName: 'Barbados',
  },
  {
    code: 'BY',
    name: 'Belarus',
    nativeName: 'Белару́сь',
  },
  {
    code: 'BE',
    name: 'Belgium',
    nativeName: 'België',
  },
  {
    code: 'BZ',
    name: 'Belize',
    nativeName: 'Belize',
  },
  {
    code: 'BJ',
    name: 'Benin',
    nativeName: 'Bénin',
  },
  {
    code: 'BM',
    name: 'Bermuda',
    nativeName: 'Bermuda',
  },
  {
    code: 'BT',
    name: 'Bhutan',
    nativeName: 'ʼbrug-yul',
  },
  {
    code: 'BO',
    name: 'Bolivia',
    nativeName: 'Bolivia',
  },
  {
    code: 'BQ',
    name: 'Bonaire, Sint Eustatius and Saba',
    nativeName: 'Bonaire',
  },
  {
    code: 'BA',
    name: 'Bosnia and Herzegovina',
    nativeName: 'Bosna i Hercegovina',
  },
  {
    code: 'BW',
    name: 'Botswana',
    nativeName: 'Botswana',
  },
  {
    code: 'BV',
    name: 'Bouvet Island',
    nativeName: 'Bouvetøya',
  },
  {
    code: 'BR',
    name: 'Brazil',
    nativeName: 'Brasil',
  },
  {
    code: 'IO',
    name: 'British Indian Ocean Territory',
    nativeName: 'British Indian Ocean Territory',
  },
  {
    code: 'BN',
    name: 'Brunei Darussalam',
    nativeName: 'Negara Brunei Darussalam',
  },
  {
    code: 'BG',
    name: 'Bulgaria',
    nativeName: 'България',
  },
  {
    code: 'BF',
    name: 'Burkina Faso',
    nativeName: 'Burkina Faso',
  },
  {
    code: 'BI',
    name: 'Burundi',
    nativeName: 'Burundi',
  },
  {
    code: 'KH',
    name: 'Cambodia',
    nativeName: 'Kâmpŭchéa',
  },
  {
    code: 'CM',
    name: 'Cameroon',
    nativeName: 'Cameroon',
  },
  {
    code: 'CA',
    name: 'Canada',
    nativeName: 'Canada',
  },
  {
    code: 'CV',
    name: 'Cape Verde',
    nativeName: 'Cabo Verde',
  },
  {
    code: 'KY',
    name: 'Cayman Islands',
    nativeName: 'Cayman Islands',
  },
  {
    code: 'CF',
    name: 'Central African Republic',
    nativeName: 'Ködörösêse tî Bêafrîka',
  },
  {
    code: 'TD',
    name: 'Chad',
    nativeName: 'Tchad',
  },
  {
    code: 'CL',
    name: 'Chile',
    nativeName: 'Chile',
  },
  {
    code: 'CN',
    name: 'China',
    nativeName: '中国',
  },
  {
    code: 'CX',
    name: 'Christmas Island',
    nativeName: 'Christmas Island',
  },
  {
    code: 'CC',
    name: 'Cocos (Keeling) Islands',
    nativeName: 'Cocos (Keeling) Islands',
  },
  {
    code: 'CO',
    name: 'Colombia',
    nativeName: 'Colombia',
  },
  {
    code: 'KM',
    name: 'Comoros',
    nativeName: 'Komori',
  },
  {
    code: 'CG',
    name: 'Congo',
    nativeName: 'République du Congo',
  },
  {
    code: 'CD',
    name: 'Congo, the Democratic Republic of the',
    nativeName: 'République démocratique du Congo',
  },
  {
    code: 'CK',
    name: 'Cook Islands',
    nativeName: 'Cook Islands',
  },
  {
    code: 'CR',
    name: 'Costa Rica',
    nativeName: 'Costa Rica',
  },
  {
    code: 'CI',
    name: "Côte d'Ivoire",
    nativeName: "Côte d'Ivoire",
  },
  {
    code: 'HR',
    name: 'Croatia',
    nativeName: 'Hrvatska',
  },
  {
    code: 'CU',
    name: 'Cuba',
    nativeName: 'Cuba',
  },
  {
    code: 'CW',
    name: 'Curaçao',
    nativeName: 'Curaçao',
  },
  {
    code: 'CY',
    name: 'Cyprus',
    nativeName: 'Κύπρος',
  },
  {
    code: 'CZ',
    name: 'Czech Republic',
    nativeName: 'Česká republika',
  },
  {
    code: 'DK',
    name: 'Denmark',
    nativeName: 'Danmark',
  },
  {
    code: 'DJ',
    name: 'Djibouti',
    nativeName: 'Djibouti',
  },
  {
    code: 'DM',
    name: 'Dominica',
    nativeName: 'Dominica',
  },
  {
    code: 'DO',
    name: 'Dominican Republic',
    nativeName: 'República Dominicana',
  },
  {
    code: 'EC',
    name: 'Ecuador',
    nativeName: 'Ecuador',
  },
  {
    code: 'EG',
    name: 'Egypt',
    nativeName: 'مصر‎',
  },
  {
    code: 'SV',
    name: 'El Salvador',
    nativeName: 'El Salvador',
  },
  {
    code: 'GQ',
    name: 'Equatorial Guinea',
    nativeName: 'Guinea Ecuatorial',
  },
  {
    code: 'ER',
    name: 'Eritrea',
    nativeName: 'ኤርትራ',
  },
  {
    code: 'EE',
    name: 'Estonia',
    nativeName: 'Eesti',
  },
  {
    code: 'ET',
    name: 'Ethiopia',
    nativeName: 'ኢትዮጵያ',
  },
  {
    code: 'FK',
    name: 'Falkland Islands (Malvinas)',
    nativeName: 'Falkland Islands',
  },
  {
    code: 'FO',
    name: 'Faroe Islands',
    nativeName: 'Føroyar',
  },
  {
    code: 'FJ',
    name: 'Fiji',
    nativeName: 'Fiji',
  },
  {
    code: 'FI',
    name: 'Finland',
    nativeName: 'Suomi',
  },
  {
    code: 'FR',
    name: 'France',
    nativeName: 'France',
  },
  {
    code: 'GF',
    name: 'French Guiana',
    nativeName: 'Guyane française',
  },
  {
    code: 'PF',
    name: 'French Polynesia',
    nativeName: 'Polynésie française',
  },
  {
    code: 'TF',
    name: 'French Southern Territories',
    nativeName: 'Territoire des Terres australes et antarctiques fr',
  },
  {
    code: 'GA',
    name: 'Gabon',
    nativeName: 'Gabon',
  },
  {
    code: 'GM',
    name: 'Gambia',
    nativeName: 'Gambia',
  },
  {
    code: 'GE',
    name: 'Georgia',
    nativeName: 'საქართველო',
  },
  {
    code: 'DE',
    name: 'Germany',
    nativeName: 'Deutschland',
    suggested: true,
  },
  {
    code: 'GH',
    name: 'Ghana',
    nativeName: 'Ghana',
  },
  {
    code: 'GI',
    name: 'Gibraltar',
    nativeName: 'Gibraltar',
  },
  {
    code: 'GR',
    name: 'Greece',
    nativeName: 'Ελλάδα',
  },
  {
    code: 'GL',
    name: 'Greenland',
    nativeName: 'Kalaallit Nunaat',
  },
  {
    code: 'GD',
    name: 'Grenada',
    nativeName: 'Grenada',
  },
  {
    code: 'GP',
    name: 'Guadeloupe',
    nativeName: 'Guadeloupe',
  },
  {
    code: 'GU',
    name: 'Guam',
    nativeName: 'Guam',
  },
  {
    code: 'GT',
    name: 'Guatemala',
    nativeName: 'Guatemala',
  },
  {
    code: 'GG',
    name: 'Guernsey',
    nativeName: 'Guernsey',
  },
  {
    code: 'GN',
    name: 'Guinea',
    nativeName: 'Guinée',
  },
  {
    code: 'GW',
    name: 'Guinea-Bissau',
    nativeName: 'Guiné-Bissau',
  },
  {
    code: 'GY',
    name: 'Guyana',
    nativeName: 'Guyana',
  },
  {
    code: 'HT',
    name: 'Haiti',
    nativeName: 'Haïti',
  },
  {
    code: 'HM',
    name: 'Heard Island and McDonald Islands',
    nativeName: 'Heard Island and McDonald Islands',
  },
  {
    code: 'VA',
    name: 'Holy See (Vatican City State)',
    nativeName: 'Sancta Sedes',
  },
  {
    code: 'HN',
    name: 'Honduras',
    nativeName: 'Honduras',
  },
  {
    code: 'HK',
    name: 'Hong Kong',
    nativeName: '香港',
  },
  {
    code: 'HU',
    name: 'Hungary',
    nativeName: 'Magyarország',
  },
  {
    code: 'IS',
    name: 'Iceland',
    nativeName: 'Ísland',
  },
  {
    code: 'IN',
    name: 'India',
    nativeName: 'भारत',
  },
  {
    code: 'ID',
    name: 'Indonesia',
    nativeName: 'Indonesia',
  },
  {
    code: 'IR',
    name: 'Iran, Islamic Republic of',
    nativeName: 'ایران',
  },
  {
    code: 'IQ',
    name: 'Iraq',
    nativeName: 'العراق',
  },
  {
    code: 'IE',
    name: 'Ireland',
    nativeName: 'Éire',
  },
  {
    code: 'IM',
    name: 'Isle of Man',
    nativeName: 'Isle of Man',
  },
  {
    code: 'IL',
    name: 'Israel',
    nativeName: 'יִשְׂרָאֵל',
  },
  {
    code: 'IT',
    name: 'Italy',
    nativeName: 'Italia',
  },
  {
    code: 'JM',
    name: 'Jamaica',
    nativeName: 'Jamaica',
  },
  {
    code: 'JP',
    name: 'Japan',
    nativeName: '日本',
  },
  {
    code: 'JE',
    name: 'Jersey',
    nativeName: 'Jersey',
  },
  {
    code: 'JO',
    name: 'Jordan',
    nativeName: 'الأردن',
  },
  {
    code: 'KZ',
    name: 'Kazakhstan',
    nativeName: 'Қазақстан',
  },
  {
    code: 'KE',
    name: 'Kenya',
    nativeName: 'Kenya',
  },
  {
    code: 'KI',
    name: 'Kiribati',
    nativeName: 'Kiribati',
  },
  {
    code: 'KP',
    name: "Korea, Democratic People's Republic of",
    nativeName: '북한',
  },
  {
    code: 'KR',
    name: 'Korea, Republic of',
    nativeName: '대한민국',
  },
  {
    code: 'KW',
    name: 'Kuwait',
    nativeName: 'الكويت',
  },
  {
    code: 'KG',
    name: 'Kyrgyzstan',
    nativeName: 'Кыргызстан',
  },
  {
    code: 'LA',
    name: "Lao People's Democratic Republic",
    nativeName: 'ສ.ປ.ປ ລາວ',
  },
  {
    code: 'LV',
    name: 'Latvia',
    nativeName: 'Latvija',
  },
  {
    code: 'LB',
    name: 'Lebanon',
    nativeName: 'لبنان',
  },
  {
    code: 'LS',
    name: 'Lesotho',
    nativeName: 'Lesotho',
  },
  {
    code: 'LR',
    name: 'Liberia',
    nativeName: 'Liberia',
  },
  {
    code: 'LY',
    name: 'Libya',
    nativeName: '‏ليبيا',
  },
  {
    code: 'LI',
    name: 'Liechtenstein',
    nativeName: 'Liechtenstein',
  },
  {
    code: 'LT',
    name: 'Lithuania',
    nativeName: 'Lietuva',
  },
  {
    code: 'LU',
    name: 'Luxembourg',
    nativeName: 'Luxembourg',
  },
  {
    code: 'MO',
    name: 'Macao',
    nativeName: '澳門',
  },
  {
    code: 'MK',
    name: 'Macedonia, the former Yugoslav Republic of',
    nativeName: 'Македонија',
  },
  {
    code: 'MG',
    name: 'Madagascar',
    nativeName: 'Madagasikara',
  },
  {
    code: 'MW',
    name: 'Malawi',
    nativeName: 'Malawi',
  },
  {
    code: 'MY',
    name: 'Malaysia',
    nativeName: 'Malaysia',
  },
  {
    code: 'MV',
    name: 'Maldives',
    nativeName: 'Maldives',
  },
  {
    code: 'ML',
    name: 'Mali',
    nativeName: 'Mali',
  },
  {
    code: 'MT',
    name: 'Malta',
    nativeName: 'Malta',
  },
  {
    code: 'MH',
    name: 'Marshall Islands',
    nativeName: 'M̧ajeļ',
  },
  {
    code: 'MQ',
    name: 'Martinique',
    nativeName: 'Martinique',
  },
  {
    code: 'MR',
    name: 'Mauritania',
    nativeName: 'موريتانيا',
  },
  {
    code: 'MU',
    name: 'Mauritius',
    nativeName: 'Maurice',
  },
  {
    code: 'YT',
    name: 'Mayotte',
    nativeName: 'Mayotte',
  },
  {
    code: 'MX',
    name: 'Mexico',
    nativeName: 'México',
  },
  {
    code: 'FM',
    name: 'Micronesia, Federated States of',
    nativeName: 'Micronesia',
  },
  {
    code: 'MD',
    name: 'Moldova, Republic of',
    nativeName: 'Moldova',
  },
  {
    code: 'MC',
    name: 'Monaco',
    nativeName: 'Monaco',
  },
  {
    code: 'MN',
    name: 'Mongolia',
    nativeName: 'Монгол улс',
  },
  {
    code: 'ME',
    name: 'Montenegro',
    nativeName: 'Црна Гора',
  },
  {
    code: 'MS',
    name: 'Montserrat',
    nativeName: 'Montserrat',
  },
  {
    code: 'MA',
    name: 'Morocco',
    nativeName: 'المغرب',
  },
  {
    code: 'MZ',
    name: 'Mozambique',
    nativeName: 'Moçambique',
  },
  {
    code: 'MM',
    name: 'Myanmar',
    nativeName: 'Myanma',
  },
  {
    code: 'NA',
    name: 'Namibia',
    nativeName: 'Namibia',
  },
  {
    code: 'NR',
    name: 'Nauru',
    nativeName: 'Nauru',
  },
  {
    code: 'NP',
    name: 'Nepal',
    nativeName: 'नेपाल',
  },
  {
    code: 'NL',
    name: 'Netherlands',
    nativeName: 'Nederland',
  },
  {
    code: 'NC',
    name: 'New Caledonia',
    nativeName: 'Nouvelle-Calédonie',
  },
  {
    code: 'NZ',
    name: 'New Zealand',
    nativeName: 'New Zealand',
  },
  {
    code: 'NI',
    name: 'Nicaragua',
    nativeName: 'Nicaragua',
  },
  {
    code: 'NE',
    name: 'Niger',
    nativeName: 'Niger',
  },
  {
    code: 'NG',
    name: 'Nigeria',
    nativeName: 'Nigeria',
  },
  {
    code: 'NU',
    name: 'Niue',
    nativeName: 'Niuē',
  },
  {
    code: 'NF',
    name: 'Norfolk Island',
    nativeName: 'Norfolk Island',
  },
  {
    code: 'MP',
    name: 'Northern Mariana Islands',
    nativeName: 'Northern Mariana Islands',
  },
  {
    code: 'NO',
    name: 'Norway',
    nativeName: 'Norge',
  },
  {
    code: 'OM',
    name: 'Oman',
    nativeName: 'عمان',
  },
  {
    code: 'PK',
    name: 'Pakistan',
    nativeName: 'Pakistan',
  },
  {
    code: 'PW',
    name: 'Palau',
    nativeName: 'Palau',
  },
  {
    code: 'PS',
    name: 'Palestine, State of',
    nativeName: 'فلسطين',
  },
  {
    code: 'PA',
    name: 'Panama',
    nativeName: 'Panamá',
  },
  {
    code: 'PG',
    name: 'Papua New Guinea',
    nativeName: 'Papua Niugini',
  },
  {
    code: 'PE',
    name: 'Peru',
    nativeName: 'Perú',
  },
  {
    code: 'PH',
    name: 'Philippines',
    nativeName: 'Pilipinas',
  },
  {
    code: 'PL',
    name: 'Poland',
    nativeName: 'Polska',
  },
  {
    code: 'PT',
    name: 'Portugal',
    nativeName: 'Portugal',
  },
  {
    code: 'PR',
    name: 'Puerto Rico',
    nativeName: 'Puerto Rico',
  },
  {
    code: 'QA',
    name: 'Qatar',
    nativeName: 'قطر',
  },
  {
    code: 'RO',
    name: 'Romania',
    nativeName: 'România',
  },
  {
    code: 'RU',
    name: 'Russia',
    nativeName: 'Россия',
  },
  {
    code: 'RW',
    name: 'Rwanda',
    nativeName: 'Rwanda',
  },
  {
    code: 'RE',
    name: 'Réunion',
    nativeName: 'La Réunion',
  },
  {
    code: 'BL',
    name: 'Saint Barthélemy',
    nativeName: 'Saint-Barthélemy',
  },
  {
    code: 'SH',
    name: 'Saint Helena',
    nativeName: 'Saint Helena',
  },
  {
    code: 'KN',
    name: 'Saint Kitts and Nevis',
    nativeName: 'Saint Kitts and Nevis',
  },
  {
    code: 'LC',
    name: 'Saint Lucia',
    nativeName: 'Saint Lucia',
  },
  {
    code: 'MF',
    name: 'Saint Martin',
    nativeName: 'Saint-Martin',
  },
  {
    code: 'PM',
    name: 'Saint Pierre and Miquelon',
    nativeName: 'Saint-Pierre-et-Miquelon',
  },
  {
    code: 'VC',
    name: 'Saint Vincent and the Grenadines',
    nativeName: 'Saint Vincent and the Grenadines',
  },
  {
    code: 'WS',
    name: 'Samoa',
    nativeName: 'Samoa',
  },
  {
    code: 'SM',
    name: 'San Marino',
    nativeName: 'San Marino',
  },
  {
    code: 'ST',
    name: 'Sao Tome and Principe',
    nativeName: 'São Tomé e Príncipe',
  },
  {
    code: 'SA',
    name: 'Saudi Arabia',
    nativeName: 'العربية السعودية',
  },
  {
    code: 'SN',
    name: 'Senegal',
    nativeName: 'Sénégal',
  },
  {
    code: 'RS',
    name: 'Serbia',
    nativeName: 'Србија',
  },
  {
    code: 'SC',
    name: 'Seychelles',
    nativeName: 'Seychelles',
  },
  {
    code: 'SL',
    name: 'Sierra Leone',
    nativeName: 'Sierra Leone',
  },
  {
    code: 'SG',
    name: 'Singapore',
    nativeName: 'Singapore',
  },
  {
    code: 'SX',
    name: 'Sint Maarten',
    nativeName: 'Sint Maarten',
  },
  {
    code: 'SK',
    name: 'Slovakia',
    nativeName: 'Slovensko',
  },
  {
    code: 'SB',
    name: 'Solomon Islands',
    nativeName: 'Solomon Islands',
  },
  {
    code: 'SO',
    name: 'Somalia',
    nativeName: 'Soomaaliya',
  },
  {
    code: 'ZA',
    name: 'South Africa',
    nativeName: 'South Africa',
  },
  {
    code: 'GS',
    name: 'South Georgia and the South Sandwich Islands',
    nativeName: 'South Georgia',
  },
  {
    code: 'SS',
    name: 'South Sudan',
    nativeName: 'South Sudan',
  },
  {
    code: 'ES',
    name: 'Spain',
    nativeName: 'España',
  },
  {
    code: 'LK',
    name: 'Sri Lanka',
    nativeName: 'śrī laṃkāva',
  },
  {
    code: 'SD',
    name: 'Sudan',
    nativeName: 'السودان',
  },
  {
    code: 'SR',
    name: 'Suriname',
    nativeName: 'Suriname',
  },
  {
    code: 'SJ',
    name: 'Svalbard and Jan Mayen',
    nativeName: 'Svalbard og Jan Mayen',
  },
  {
    code: 'SZ',
    name: 'Swaziland',
    nativeName: 'Swaziland',
  },
  {
    code: 'SE',
    name: 'Sweden',
    nativeName: 'Sverige',
  },
  {
    code: 'CH',
    name: 'Switzerland',
    nativeName: 'Schweiz',
    suggested: true,
  },
  {
    code: 'SY',
    name: 'Syria',
    nativeName: 'سوريا',
  },
  {
    code: 'TW',
    name: 'Taiwan',
    nativeName: '臺灣',
  },
  {
    code: 'TJ',
    name: 'Tajikistan',
    nativeName: 'Тоҷикистон',
  },
  {
    code: 'TZ',
    name: 'Tanzania',
    nativeName: 'Tanzania',
  },
  {
    code: 'TH',
    name: 'Thailand',
    nativeName: 'ประเทศไทย',
  },
  {
    code: 'TL',
    name: 'Timor-Leste',
    nativeName: 'Timor-Leste',
  },
  {
    code: 'TG',
    name: 'Togo',
    nativeName: 'Togo',
  },
  {
    code: 'TK',
    name: 'Tokelau',
    nativeName: 'Tokelau',
  },
  {
    code: 'TO',
    name: 'Tonga',
    nativeName: 'Tonga',
  },
  {
    code: 'TT',
    name: 'Trinidad and Tobago',
    nativeName: 'Trinidad and Tobago',
  },
  {
    code: 'TN',
    name: 'Tunisia',
    nativeName: 'تونس',
  },
  {
    code: 'TR',
    name: 'Turkey',
    nativeName: 'Türkiye',
  },
  {
    code: 'TM',
    name: 'Turkmenistan',
    nativeName: 'Türkmenistan',
  },
  {
    code: 'TC',
    name: 'Turks and Caicos Islands',
    nativeName: 'Turks and Caicos Islands',
  },
  {
    code: 'TV',
    name: 'Tuvalu',
    nativeName: 'Tuvalu',
  },
  {
    code: 'UG',
    name: 'Uganda',
    nativeName: 'Uganda',
  },
  {
    code: 'UA',
    name: 'Ukraine',
    nativeName: 'Україна',
  },
  {
    code: 'AE',
    name: 'United Arab Emirates',
    nativeName: 'دولة الإمارات العربية المتحدة',
  },
  {
    code: 'GB',
    name: 'United Kingdom',
    nativeName: 'United Kingdom',
  },
  {
    code: 'US',
    name: 'United States',
    nativeName: 'United States',
  },
  {
    code: 'UM',
    name: 'United States Minor Outlying Islands',
    nativeName: 'United States Minor Outlying Islands',
  },
  {
    code: 'UY',
    name: 'Uruguay',
    nativeName: 'Uruguay',
  },
  {
    code: 'UZ',
    name: 'Uzbekistan',
    nativeName: 'Oʻzbekiston',
  },
  {
    code: 'VU',
    name: 'Vanuatu',
    nativeName: 'Vanuatu',
  },
  {
    code: 'VE',
    name: 'Venezuela',
    nativeName: 'Venezuela',
  },
  {
    code: 'VN',
    name: 'Vietnam',
    nativeName: 'Việt Nam',
  },
  {
    code: 'VG',
    name: 'Virgin Islands, British',
    nativeName: 'British Virgin Islands',
  },
  {
    code: 'VI',
    name: 'Virgin Islands, U.S.',
    nativeName: 'U.S. Virgin Islands',
  },
  {
    code: 'WF',
    name: 'Wallis and Futuna',
    nativeName: 'Wallis et Futuna',
  },
  {
    code: 'EH',
    name: 'Western Sahara',
    nativeName: 'الصحراء الغربية',
  },
  {
    code: 'YE',
    name: 'Yemen',
    nativeName: 'اليمن',
  },
  {
    code: 'ZM',
    name: 'Zambia',
    nativeName: 'Zambia',
  },
  {
    code: 'ZW',
    name: 'Zimbabwe',
    nativeName: 'Zimbabwe',
  },
];
