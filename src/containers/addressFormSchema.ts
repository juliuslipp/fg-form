import * as Yup from 'yup';

const HOUSE_NUMBER_REGEX = /^[1-9]\d*(?:[ -]?(?:[a-zA-Z]+|[1-9]\d*))?$/;

export const validationSchema = Yup.object({
  route: Yup.string().default('').required('Straße ist ein Pflichtfeld.'),
  streetNumber: Yup.string()
    .default('')
    .required('Hausnummer ist ein Pflichtfeld.')
    .matches(HOUSE_NUMBER_REGEX, 'Invalide Hausnummer.'),
  postalCode: Yup.string().default('').required('Postleitzahl ist ein Pflichtfeld.'),
  locality: Yup.string().default('').required('Ort ist ein Pflichtfeld.'),
  sublocality: Yup.string().default('').notRequired(),
  regionCode: Yup.string().default('').required('Land ist ein Pflichtfeld.'),
});

export type FormData = Yup.InferType<typeof validationSchema>;

export const initialValues: FormData = validationSchema.getDefault();
