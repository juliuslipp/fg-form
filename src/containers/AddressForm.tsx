import React, { FC, useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Alert, Button, Grid } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { v4 as uuid } from 'uuid';
import { FGFormikInput } from '../components/FGFormikInput';
import { validateAddress } from '../services/mapsService';
import { AutocompleteOption } from '../components/FGAutocomplete';
import { COUNTRIES } from '../constants/countries';
import { getKeys } from '../utils/object';
import * as Maps from '../types/Maps';
import { handleApiError } from '../utils/axios';
import { FormData, initialValues, validationSchema } from './addressFormSchema';
import { addToStorage } from '../services/mockClient';

const TypedFormInput = FGFormikInput<FormData>;

function mapComponentTypeToSchemaKey(key: Maps.ComponentType): keyof FormData {
  switch (key) {
    case 'street_number':
      return 'streetNumber';
    case 'postal_code':
      return 'postalCode';
    case 'administrative_area':
      return 'sublocality';
    case 'country':
      return 'regionCode';
    default:
      return key;
  }
}

// List of options for the country autocomplete field.
const autoCompleteOptions: AutocompleteOption[] = COUNTRIES.map(
  ({ nativeName, code, name, suggested }) => ({
    label: nativeName,
    value: code,
    listLabel: `${nativeName} (${name})`,
    suggested,
  })
);

const AddressForm: FC = () => {
  // State for handling and displaying errors from the maps service API.
  const [apiError, setApiError] = useState<string | undefined>(undefined);
  // State for optimizing maps service requests.
  const [previousResponseId, setPreviousResponseId] = useState<string | undefined>(undefined);
  const navigate = useNavigate();

  const onSubmit = async (
    values: FormData,
    { setSubmitting, setFieldError }: FormikHelpers<FormData>
  ) => {
    setApiError(undefined);

    try {
      const { route, streetNumber, ...mapsAddress } = values;
      const { errors, responseId, suggestions, components } = await validateAddress(
        {
          ...mapsAddress,
          addressLines: [route, streetNumber],
        },
        previousResponseId
      );
      setPreviousResponseId(responseId);

      getKeys(errors).forEach((key) => {
        setFieldError(mapComponentTypeToSchemaKey(key), 'Bitte überprüfe deine Eingabe.');
      });

      if (!getKeys(errors).length) {
        const id = uuid();
        addToStorage(
          id,
          components.reduce(
            (acc, curr) => ({
              ...acc,
              [curr.componentType]: curr.componentName.text,
            }),
            {}
          )
        );
        navigate(`/address/${id}`);
      }

      if (getKeys(suggestions).length) {
        console.log('Some suggestions!', suggestions);
      }
    } catch (error) {
      setApiError(handleApiError(error));
    } finally {
      setSubmitting(false);
    }
  };

  return (
    <>
      <Formik<FormData>
        validateOnChange={false}
        validateOnBlur={false}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({ isSubmitting }) => {
          return (
            <Form>
              <Grid container spacing={1}>
                <Grid item xs={12} sm={8}>
                  <TypedFormInput
                    required
                    label="Straße"
                    name="route"
                    autoComplete="address-line1"
                  />
                </Grid>

                <Grid item xs={12} sm={4}>
                  <TypedFormInput
                    required
                    label="Hausnummer"
                    name="streetNumber"
                    autoComplete="address-line2"
                  />
                </Grid>

                <Grid item xs={12}>
                  <TypedFormInput
                    required
                    label="Postleitzahl"
                    name="postalCode"
                    autoComplete="postal-code"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TypedFormInput
                    required
                    label="Stadt"
                    name="locality"
                    autoComplete="address-level2"
                  />
                </Grid>

                <Grid item xs={12}>
                  <TypedFormInput
                    label="Stadtteil"
                    name="sublocality"
                    autoComplete="address-level3"
                  />
                </Grid>

                <Grid item xs={12}>
                  <TypedFormInput
                    label="Land"
                    name="regionCode"
                    required
                    options={autoCompleteOptions}
                  />
                </Grid>

                <Grid item xs={12} sm={4} margin="auto" marginTop={2}>
                  <Button
                    fullWidth
                    color="primary"
                    variant="contained"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    {isSubmitting ? 'Speichert...' : 'Adresse speichern'}
                  </Button>
                </Grid>
              </Grid>
            </Form>
          );
        }}
      </Formik>
      {apiError && (
        <Alert variant="outlined" severity="error">
          Sadly, an error occurred: {apiError}
        </Alert>
      )}
    </>
  );
};

export default AddressForm;
