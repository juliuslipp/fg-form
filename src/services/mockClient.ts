const store: object = {};

export function addToStorage(key: string, value: any) {
  // @ts-ignore
  store[key] = value;
}

export function getFromStorage(key: string) {
  // @ts-ignore
  return store[key];
}
