import axios from 'axios';
import * as Maps from '../types/Maps';

const API_KEY = 'YOUR_API_KEY';

export const postMapsAddressValidation = async (
  address: Maps.Address,
  previousResponseId?: string
): Promise<Maps.ValidationResponse> => {
  const result = await axios.post<Maps.ValidationResponse>(
    `https://addressvalidation.googleapis.com/v1:validateAddress`,
    {
      address,
      previousResponseId,
    },
    {
      params: {
        key: API_KEY,
      },
    }
  );

  return result.data;
};
