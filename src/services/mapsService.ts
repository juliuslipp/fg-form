import { Address, AddressComponent, ComponentType, ConfirmationType } from '../types/Maps';
import { postMapsAddressValidation } from './mapsApiService';
import { KeyOfType } from '../types/Common';

export enum CorrectionType {
  INFERRED = 'INFERRED',
  SPELL_CORRECTED = 'SPELL_CORRECTED',
  REPLACED = 'REPLACED',
}

export interface ValidationResult {
  errors: KeyOfType<ComponentType, ConfirmationType>;
  suggestions: KeyOfType<
    ComponentType,
    {
      type: CorrectionType;
      value: string;
    }
  >;
  components: AddressComponent[];
  responseId: string;
}

export const validateAddress = async (
  address: Address,
  previousResponseId?: string
): Promise<ValidationResult> => {
  const { result, responseId } = await postMapsAddressValidation(address, previousResponseId);
  const validationResult: ValidationResult = {
    responseId,
    errors: {},
    suggestions: {},
    components: result.address.addressComponents,
  };

  result.address.addressComponents.forEach(
    ({ confirmationLevel, componentName, replaced, componentType, spellCorrected, inferred }) => {
      if (confirmationLevel !== ConfirmationType.CONFIRMED) {
        validationResult.errors[componentType] = confirmationLevel;
      } else if (spellCorrected) {
        validationResult.suggestions[componentType] = {
          type: CorrectionType.SPELL_CORRECTED,
          value: componentName.text,
        };
      } else if (inferred) {
        validationResult.suggestions[componentType] = {
          type: CorrectionType.INFERRED,
          value: componentName.text,
        };
      } else if (replaced) {
        validationResult.suggestions[componentType] = {
          type: CorrectionType.REPLACED,
          value: componentName.text,
        };
      }
    }
  );
  return validationResult;
};
