import { useField } from 'formik';
import { Info as InfoIcon } from '@mui/icons-material';
import React from 'react';
import { styled } from '@mui/material';
import FGTextField, { FGTextFieldProps } from './FGTextField';
import { AutocompleteOption, FGAutocomplete } from './FGAutocomplete';

interface FormBaseInputProps<T> {
  label: string;
  name: keyof T & string;
  options?: AutocompleteOption[];
}

const HelperText = styled('span')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  svg: {
    marginRight: theme.spacing(0.5),
    fontSize: 14,
  },
}));

export type FormInputProps<T> = FormBaseInputProps<T> & FGTextFieldProps;

const getErrorHelperText = (error: string) => (
  <HelperText>
    <InfoIcon />
    {error}
  </HelperText>
);

export function FGFormikInput<Schema>({
  label,
  options,
  required,
  helperText,
  name,
  value,
  ...props
}: FormInputProps<Schema>) {
  const [field, meta, helpers] = useField(name);

  const { touched, error } = meta;
  const showError = touched && !!error;
  const TextFieldProps: FGTextFieldProps = {
    error: showError,
    helperText: showError ? getErrorHelperText(error) : helperText,
    label: required ? `${label}*` : label,
    'aria-errormessage': showError ? error : undefined,
    'aria-label': label,
    'aria-required': required,
    'aria-invalid': showError,
    ...props,
  };

  return !options ? (
    <FGTextField {...field} {...TextFieldProps} />
  ) : (
    <FGAutocomplete
      value={options.find((option) => option.value === field.value) ?? null}
      options={options}
      TextFieldProps={TextFieldProps}
      onChange={(_, value) => {
        helpers.setValue(value?.value || '');
      }}
      onBlur={() => {
        helpers.setTouched(true);
      }}
    />
  );
}
