import { TextField, TextFieldProps, useTheme } from '@mui/material';
import React from 'react';

export type FGTextFieldProps = Omit<TextFieldProps, 'variant'>;

function FGTextField({
  InputProps,
  InputLabelProps,
  FormHelperTextProps,
  ...props
}: FGTextFieldProps) {
  const theme = useTheme();

  return (
    <TextField
      fullWidth
      InputProps={{
        disableUnderline: true,
        ...InputProps,
        sx: {
          borderRadius: '8px',
          '&.Mui-error': {
            border: `1px solid ${theme.palette.error.main}`,
            boxSizing: 'border-box',
          },
          ...InputProps?.sx,
        },
      }}
      FormHelperTextProps={{
        ...FormHelperTextProps,
        sx: {
          marginLeft: 0,
        },
      }}
      InputLabelProps={{
        ...InputLabelProps,
        sx: {
          '&.Mui-focused': {
            color: props.error ? 'error.main' : 'text.secondary',
          },
          ...InputLabelProps?.sx,
        },
      }}
      variant="filled"
      {...props}
    />
  );
}

export default FGTextField;
