import { Autocomplete, AutocompleteProps, Box, createFilterOptions } from '@mui/material';
import React, { useMemo } from 'react';
import FGTextField, { FGTextFieldProps } from './FGTextField';

export interface AutocompleteOption {
  label: string;
  listLabel: string;
  value: string;
  suggested?: boolean;
}

interface FGAutocompleteProps
  extends Omit<
    AutocompleteProps<AutocompleteOption, undefined, any, undefined>,
    'renderInput' | 'filterOptions' | 'renderOption' | 'multiple'
  > {
  TextFieldProps?: FGTextFieldProps;
}

const renderOptions = (props: React.HTMLAttributes<HTMLLIElement>, option: AutocompleteOption) => (
  <Box component="li" {...props}>
    {option.listLabel}
  </Box>
);

const filterOptions = createFilterOptions<AutocompleteOption>({
  ignoreCase: true,
  stringify: (option) => `${option.label} ${option.value} ${option.listLabel}`,
});

export function FGAutocomplete({ options, TextFieldProps, ...props }: FGAutocompleteProps) {
  const prioOptions = useMemo(
    () =>
      [...options].sort((a, b) => {
        if (a.suggested && !b.suggested) {
          return -1;
        }
        if (!a.suggested && b.suggested) {
          return 1;
        }
        return 0;
      }),
    [options]
  );

  return (
    <Autocomplete<AutocompleteOption>
      fullWidth
      autoHighlight
      options={prioOptions}
      filterOptions={filterOptions}
      renderOption={renderOptions}
      getOptionLabel={(option) => option.label}
      renderInput={(params) => (
        <FGTextField
          {...TextFieldProps}
          {...params}
          inputProps={{
            ...params.inputProps,
            ...TextFieldProps?.inputProps,
            autoComplete: 'whatever', // disable autocomplete and autofill
          }}
        />
      )}
      {...props}
    />
  );
}
