import { useParams } from 'react-router-dom';
import { getFromStorage } from '../services/mockClient';

function AddressViewPage() {
  const { id } = useParams();
  return <div>{id && JSON.stringify(getFromStorage(id))}</div>;
}

export default AddressViewPage;
