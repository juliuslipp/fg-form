import { Paper, styled } from '@mui/material';
import AddressForm from '../containers/AddressForm';

const StyledDiv = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

const StyledPaper = styled(Paper)(({ theme }) => ({
  padding: theme.spacing(2),
  margin: theme.spacing(2),
  maxWidth: '600px',
  minWidth: '230px',
}));

function FormPage() {
  return (
    <StyledDiv>
      <StyledPaper>
        <AddressForm />
      </StyledPaper>
    </StyledDiv>
  );
}

export default FormPage;
