import { AxiosError } from 'axios';

export function handleApiError(error: any) {
  if (error instanceof AxiosError) {
    return error.message;
  }

  throw error;
}
