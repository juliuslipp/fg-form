import React from 'react';
import './App.css';
import { createTheme, ThemeProvider } from '@mui/material';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import FormPage from './pages/FormPage';
import AddressViewPage from './pages/AddressViewPage';

const theme = createTheme({
  palette: {
    primary: {
      main: '#10fad8',
      contrastText: '#3c318f',
    },
    text: {
      primary: '#3c318f',
    },
    background: {
      paper: '#f2f1f6',
    },
  },
  typography: {
    button: {
      textTransform: 'none',
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Routes>
          <Route key="address-form" path="/" element={<FormPage />} />
          <Route key="address-view" path="/address/:id" element={<AddressViewPage />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
