export type KeyOfType<K extends symbol | string | number, V> = {
  [key in K]?: V;
};
