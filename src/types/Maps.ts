export enum ConfirmationType {
  CONFIRMED = 'CONFIRMED',
  UNCONFIRMED = 'UNCONFIRMED',
}

export interface Address {
  // Country code. For example, "DE".
  regionCode?: string;
  // Postal code. For example, "70563".
  postalCode?: string;
  // Administrative area. For example, "Baden-Württemberg".
  administrativeArea?: string;
  // Locality. For example, "Stuttgart".
  locality?: string;
  // Sublocality. For example, "Vaihingen".
  sublocality?: string;
  // Full street name. For example, ["Königsstraße", "3"].
  addressLines: string[];
}

export type ComponentType =
  | keyof Omit<Address, 'addressLines' | 'administrativeArea' | 'postalCode' | 'regionCode'>
  | 'administrative_area'
  | 'postal_code'
  | 'street_number'
  | 'route'
  | 'country';

export interface AddressComponent {
  componentName: {
    text: string;
    languageCode?: string;
  };
  componentType: ComponentType;
  confirmationLevel: ConfirmationType;
  inferred?: boolean;
  replaced?: boolean;
  spellCorrected?: boolean;
}

export interface Verdict {
  addressComplete: boolean;
  hasInferredComponents: boolean;
  hasReplacedComponents: boolean;
  hasUnconfirmedComponents: boolean;
}

export interface ValidationResponse {
  result: {
    verdict: Verdict;
    address: {
      formattedAddress: string;
      postalAddress: Address;
      unconfirmedComponentTypes: ComponentType[];
      addressComponents: AddressComponent[];
    };
  };
  responseId: string;
}
